'use strict'

// Get Quotes from API
const quoteContainer = document.getElementById('quote-container');
const quoteText = document.getElementById('quote');
const authorText = document.getElementById('author');
const twitterBtn = document.getElementById('twitter');
const newQuoteBtn = document.getElementById('new-quote');
const loader = document.getElementById('loader');
const errorContainer = document.getElementById('error-container');
const errorText = document.getElementById('error-text');

let apiQuotes = [];
let errorCounter = 0;

// Show Loading
function showLoader() {
    loader.hidden = false;
    quoteContainer.hidden = true;
}

// Hide loader
function hideLoader() {
    loader.hidden = true;
    quoteContainer.hidden = false;
}


// Show new Quote
function newQuote() {
    const quote = apiQuotes[Math.floor(Math.random() * apiQuotes.length)];

    showLoader();

    authorText.textContent = (quote.author) ? quote.author : 'Unknown';

    if (quote.text.length > 120) {
        quoteText.classList.add('long-quote');
        console.log('Long quote added');
    }
    else {
        quoteText.classList.remove('long-quote');
        console.log('Long quote removed');
    }

    quoteText.textContent = quote.text;

    hideLoader();
}

async function getQuotes() {
    const apiURL = 'https://type.fit/api/quotes';

    errorContainer.hidden = true;

    try {
        showLoader();
        const response = await fetch(apiURL);
        apiQuotes = await response.json();
        newQuote();

    } catch (error) {
        if (errorCounter < 10) {
            getQuotes();
            errorCounter++;
        }
        else {
            errorContainer.hidden = false;
            hideLoader();
            quoteContainer.hidden = true;
            errorText.textContent = "Error 404";
        }
    }
}


// Tweet Quote
function tweetQuote() {
    const twitterUrl = `https://twitter.com/intent/tweet?text=${quoteText.textContent} - ${authorText.textContent}`;
    window.open(twitterUrl, '_blank');
}


// Event listeners
newQuoteBtn.addEventListener('click', newQuote);
twitterBtn.addEventListener('click', tweetQuote);


getQuotes();

